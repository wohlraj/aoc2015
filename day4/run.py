#!/usr/bin/env python3
import hashlib

# in_file = 'example'
in_file = 'input'

with open(in_file) as f:
	word = f.read().splitlines()[0]

suffix = 1
while hashlib.md5((word+str(suffix)).encode()).hexdigest()[0:5] != "00000":
	suffix += 1
print(str(suffix))
while hashlib.md5((word+str(suffix)).encode()).hexdigest()[0:6] != "000000":
	suffix += 1
print(str(suffix))
