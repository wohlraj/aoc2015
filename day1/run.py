#!/usr/bin/env python3
in_file = 'input'

with open(in_file) as f:
	line = f.read().splitlines()[0]
	floors, floor = [0], 0
	for c in line:
		if c == '(':
			floor += 1
		else:
			floor -= 1
		floors.append(floor)

print(floors[-1])
print(floors.index(-1))
