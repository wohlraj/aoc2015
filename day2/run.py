#!/usr/bin/env python3

in_file = 'input'
with open(in_file) as f:
	lines = f.read().splitlines()

paper = ribbon = 0
for li in lines:
	l,w,h = li.split('x')
	l,w,h = int(l),int(w),int(h)
	paper += 2*l*w+2*w*h+2*h*l + min(l*w,w*h,h*l)
	ribbon += sum(sorted([l,w,h])[:2])*2 + l*w*h
print(paper)
print(ribbon)
