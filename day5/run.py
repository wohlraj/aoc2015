#!/usr/bin/env python3

# in_file = 'example'
in_file = 'input'

# part 1
vowels = ['a','e','i','o','u']
with open(in_file) as f:
	lines = f.read().splitlines()
	nice = 0
	for l in lines:

		# 3 vowels or more
		vcount = 0
		for v in vowels:
			vcount += l.count(v)
		if vcount < 3:
			continue

		# 2 consecutive chars
		res = False
		for a,b in zip(l[0:-1],l[1:]):
			if a == b:
				res = True
				break
		if not res:
			continue

		# string contains
		if "ab" in l or "cd" in l or "pq" in l or "xy" in l:
			continue

		nice += 1
print(nice)

# part 2
with open(in_file) as f:
	lines = f.read().splitlines()
	nice = 0
	for l in lines:
		# pairs of two
		for i in range(len(l)-2):
			res = False
			if l[i:i+2] in l[0:i] or l[i:i+2] in l[i+2::]:
				res = True
				break
		if not res:
			continue

		# one letter which repeats
		for i in range(len(l)-2):
			res = False
			if l[i] == l[i+2]:
				res = True
				break
		if not res:
			continue

		nice += 1
print(nice)
