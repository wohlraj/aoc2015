# caching

```python
import functools

@functools.cache
def func(hashable_vars...):
```

# sorting

```python
queue = sorted(queue,key=lambda a:(a[0]['ge'],a[0]['ob'],a[0]['cl'],a[0]['or']),reverse=True)
```

# search algorithms

* BFS: Breadth First Search, FIFO storage
* DFS: Depth First Search, LIFO storage
  * both can optionnaly keep track of traversed path
* Dijkstra: can be BFS or DFS-based, adds weights on edges
* A*: adds heuristics to Dijkstra

* generic search code:

```python
def find_path(start, end, grid):
    open_list = set()
    open_list.add(start)
    closed_list = set()
    parents = {}
    parents[start] = start
    dists = {}
    dists[start] = 0

    while len(open_list):
        node = None
        for n in open_list:
            if node == None or dists[n] < dists[node]:
                node = n

        for nb in directions: # pick all directions
            next_node = tuple([node[0]+nb[0],node[1]+nb[1]])
            if not_out_of_bounds():
                if not_allowed_path():
                    continue
                elif next_node == end: # arrived at destination
                    return dists[node] + 1
                elif not next_node in closed_list:
                    open_list.add(next_node)
                    parents[next_node] = node
                    dists[next_node] = dists[node] + 1
        open_list.remove(node)
        closed_list.add(node)
```

# misc

* Floyd-Warshall: computes distances between nodes

```python
distances = c.defaultdict(lambda: 1000)
for v1 in data:
    for v2 in data[v1]['dest']:
        distances[v1,v2] = 1
for k, i, j in itertools.product(data,data,data):
    distances[i,j] = min(distances[i,j], distances[i,k] + distances[k,j])
```
