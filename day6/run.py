#!/usr/bin/env python3
from collections import defaultdict
import itertools
import re

in_file = 'input'
r = r'(\w+) (\d*),(\d*) .* (\d*),(\d*)'

# part 1
grid = defaultdict(bool)
for instr,x1,y1,x2,y2 in re.findall(r, open(in_file).read()):
	x1,y1,x2,y2 = int(x1),int(y1),int(x2),int(y2)
	if instr in ['on','off']:
		for i,j in itertools.product(range(x1,x2+1),range(y1,y2+1)):
			grid[i+j*1j] = instr == 'on'
	elif instr == "toggle":
		for i,j in itertools.product(range(x1,x2+1),range(y1,y2+1)):
			grid[i+j*1j] = not grid[i+j*1j]
res = 0
for i,j in itertools.product(range(1000),range(1000)):
	if grid[i+j*1j]:
		res += 1
print(res)

# part 2
grid = defaultdict(int)
for instr,x1,y1,x2,y2 in re.findall(r, open(in_file).read()):
	x1,y1,x2,y2 = int(x1),int(y1),int(x2),int(y2)
	if instr == "on":
		for i,j in itertools.product(range(x1,x2+1),range(y1,y2+1)):
			grid[i+j*1j] += 1
	elif instr == "off":
		for i,j in itertools.product(range(x1,x2+1),range(y1,y2+1)):
			grid[i+j*1j] = max(grid[i+j*1j]-1, 0)
	elif instr == "toggle":
		for i,j in itertools.product(range(x1,x2+1),range(y1,y2+1)):
			grid[i+j*1j] += 2
res = 0
for i,j in itertools.product(range(1000),range(1000)):
	res += grid[i+j*1j]
print(res)
