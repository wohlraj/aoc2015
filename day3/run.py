#!/usr/bin/env python3

in_file = 'input'
with open(in_file) as f:
	lines = f.read().splitlines()
	for l in lines:
		pos = pos_robo = 0+0j
		visited = set()
		visited.add(pos)
		i = 0
		for c in l:
			if c == '>':
				if i%2:
					pos_robo += 1
					visited.add(pos_robo)
				else:
					pos += 1
					visited.add(pos)
			elif c == '<':
				if i%2:
					pos_robo -= 1
					visited.add(pos_robo)
				else:
					pos -= 1
					visited.add(pos)
			elif c == '^':
				if i%2:
					pos_robo += 1j
					visited.add(pos_robo)
				else:
					pos += 1j
					visited.add(pos)
			elif c == 'v':
				if i%2:
					pos_robo -= 1j
					visited.add(pos_robo)
				else:
					pos -= 1j
					visited.add(pos)
			i += 1
print(len(visited))
